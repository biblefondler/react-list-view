# React List View
An app to pull in data from BBC News feed, display in list view
## Notes
* Ruby Compass is used for compiling SCSS into CSS, configured via the config.rb file.
    * Running the command "compass watch" from the Ruby console will automatically compile SCSS into CSS on file save.
* PHP is needed for the cross-domain AJAX calls to BBC, this functionality was built using PHP 5.6.23, but should work in any version of PHP 5.
* JSX syntax is used for React implementation, Babel is used for transpilation, dependencies are listed in package.json.
## Tools used
* NodeJS
* Babel
* Compass
* React
* React DOM
