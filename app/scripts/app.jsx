var NewsItem = React.createClass({
  fetchItemDetails: function(){
    $.ajax({
      url: this.props.itemDetailURL,
      data: {url: this.props.data.guid},
      dataType: 'html',
      cache: false,
      method: "GET",
      success: function(data) {
        var storyData = $(data).find(".story-body__inner").html();
        if (!storyData.length){
          storyData = data;
        }
        this.setState({
          detailHTML:storyData,
          buttonText:"[close]",
          detailClass: "detail-view active",
          showDetail: true
        });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleDetailShow: function(){
    if (this.state.showDetail){
      this.setState({
        detailHTML: "<div className=\"loader\"><i className=\"fa fa-circle-o-notch fa-spin\"></i></div>",
        buttonText: "[view]",
        detailClass: "detail-view",
        showDetail: false
      });
    }else{
      this.setState({
        buttonText:"[loading]",
        detailClass: "detail-view pending"
      });
      this.fetchItemDetails();
    }
  },
  getInitialState: function() {
    return {
      detailHTML: "<div class=\"loader\"><i class=\"fa fa-circle-o-notch fa-spin\"></i></div>",
      buttonText: "[view]",
      detailClass: "detail-view",
      showDetail: false
    };
  },
  render: function(){
    return (
      <li className="news-item">
        <h3 className="title">{this.props.data.title}</h3>
        <div className="pub-date">{this.props.data.pubDate}</div>
        <div className="description">{this.props.data.description}</div>
        <button className="detail-button" onClick={this.handleDetailShow}>{this.state.buttonText}</button>
        <div className={this.state.detailClass} dangerouslySetInnerHTML={{__html : this.state.detailHTML}} />
      </li>
    );
  }
});

var NewsFeed = React.createClass({
  fetchFeed: function(){
    $.ajax({
      url: this.props.newsFeedURL,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var items=$.map(data.item, function(value, index) {
            return [value];
        });
        this.setState({
          raw_feed: data,
          feed_items: items,
          updateDate: data.lastBuildDate
        });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {
      raw_feed: {},
      feed_items: [],
      updateDate: ""
    };
  },
  componentDidMount: function() {
    this.fetchFeed();
    setInterval(this.fetchFeed, this.props.pollInterval);
  },
  render: function(){
    var itemNodes = this.state.feed_items.map(function(item) {
      return (
        <NewsItem data={item} key={item.guid} itemDetailURL="./includes/fetch-news-item.php" />
      );
    });
    return (
      <div id="news-feed">
        <h1>BBC World News Feed</h1>
        <h2>Last Updated: {this.state.updateDate}</h2>
        <ul id="item-list">
          {itemNodes}
        </ul>
      </div>
    );
  }
});

ReactDOM.render(
  <NewsFeed newsFeedURL="./includes/fetch-news-feed.php" pollInterval={10000} />,
  document.getElementById('root')
);
