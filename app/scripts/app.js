"use strict";

var NewsItem = React.createClass({
  displayName: "NewsItem",

  fetchItemDetails: function fetchItemDetails() {
    $.ajax({
      url: this.props.itemDetailURL,
      data: { url: this.props.data.guid },
      dataType: 'html',
      cache: false,
      method: "GET",
      success: function (data) {
        var storyData = $(data).find(".story-body__inner").html();
        if (!storyData.length) {
          storyData = data;
        }
        this.setState({
          detailHTML: storyData,
          buttonText: "[close]",
          detailClass: "detail-view active",
          showDetail: true
        });
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleDetailShow: function handleDetailShow() {
    if (this.state.showDetail) {
      this.setState({
        detailHTML: "<div className=\"loader\"><i className=\"fa fa-circle-o-notch fa-spin\"></i></div>",
        buttonText: "[view]",
        detailClass: "detail-view",
        showDetail: false
      });
    } else {
      this.setState({
        buttonText: "[loading]",
        detailClass: "detail-view pending"
      });
      this.fetchItemDetails();
    }
  },
  getInitialState: function getInitialState() {
    return {
      detailHTML: "<div class=\"loader\"><i class=\"fa fa-circle-o-notch fa-spin\"></i></div>",
      buttonText: "[view]",
      detailClass: "detail-view",
      showDetail: false
    };
  },
  render: function render() {
    return React.createElement(
      "li",
      { className: "news-item" },
      React.createElement(
        "h3",
        { className: "title" },
        this.props.data.title
      ),
      React.createElement(
        "div",
        { className: "pub-date" },
        this.props.data.pubDate
      ),
      React.createElement(
        "div",
        { className: "description" },
        this.props.data.description
      ),
      React.createElement(
        "button",
        { className: "detail-button", onClick: this.handleDetailShow },
        this.state.buttonText
      ),
      React.createElement("div", { className: this.state.detailClass, dangerouslySetInnerHTML: { __html: this.state.detailHTML } })
    );
  }
});

var NewsFeed = React.createClass({
  displayName: "NewsFeed",

  fetchFeed: function fetchFeed() {
    $.ajax({
      url: this.props.newsFeedURL,
      dataType: 'json',
      cache: false,
      success: function (data) {
        var items = $.map(data.item, function (value, index) {
          return [value];
        });
        this.setState({
          raw_feed: data,
          feed_items: items,
          updateDate: data.lastBuildDate
        });
      }.bind(this),
      error: function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function getInitialState() {
    return {
      raw_feed: {},
      feed_items: [],
      updateDate: ""
    };
  },
  componentDidMount: function componentDidMount() {
    this.fetchFeed();
    setInterval(this.fetchFeed, this.props.pollInterval);
  },
  render: function render() {
    var itemNodes = this.state.feed_items.map(function (item) {
      return React.createElement(NewsItem, { data: item, key: item.guid, itemDetailURL: "./includes/fetch-news-item.php" });
    });
    return React.createElement(
      "div",
      { id: "news-feed" },
      React.createElement(
        "h1",
        null,
        "BBC World News Feed"
      ),
      React.createElement(
        "h2",
        null,
        "Last Updated: ",
        this.state.updateDate
      ),
      React.createElement(
        "ul",
        { id: "item-list" },
        itemNodes
      )
    );
  }
});

ReactDOM.render(React.createElement(NewsFeed, { newsFeedURL: "./includes/fetch-news-feed.php", pollInterval: 10000 }), document.getElementById('root'));
//# sourceMappingURL=C:\xampp\htdocs\dashboard\Projects\react-list-view\app\scripts\app.js.map