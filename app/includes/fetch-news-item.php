<?php
/* * * * * * * * * * *
 * This file pulls feed info for a given article
 */

//cleans XML for UTF8 char encoding
function utf8_for_xml($string){
	return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
}

if (isset($_GET['url'])){
  $article_url = $_GET['url'];

  $ch = curl_init();
  /* If you want to send an XML Request, use these options */
  curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt( $ch,CURLOPT_SSL_VERIFYHOST,false);
  curl_setopt( $ch, CURLOPT_HTTPHEADER,  array( 'Content-type: APPLICATION/XML; CHARSET=UTF-8' ) );
  curl_setopt( $ch, CURLOPT_URL, $article_url );
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

  if(curl_exec($ch) === false){
    echo 'Curl Error:' . curl_error($ch);
  }else{
    $result = curl_exec( $ch );
  }
  curl_close($ch);

  //display result
  echo $result;
}
?>
