<?php
/* * * * * * * * * * *
 * This file refreshes the BBC news feed
 */

//cleans XML for UTF8 char encoding
function utf8_for_xml($string){
	return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
}

$ch = curl_init();


/* If you want to send an XML Request, use these options */
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
curl_setopt( $ch,CURLOPT_SSL_VERIFYHOST,false);
curl_setopt( $ch, CURLOPT_HTTPHEADER,  array( 'Content-type: APPLICATION/XML; CHARSET=UTF-8' ) );
curl_setopt( $ch, CURLOPT_URL, "http://feeds.bbci.co.uk/news/world/rss.xml?edition=uk" );
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

if(curl_exec($ch) === false){
	echo 'Curl Error:' . curl_error($ch);
}else{
	$result = curl_exec( $ch );
}
curl_close($ch);

// convert retrieved xml to json
$fileContents = str_replace(array("\n", "\r", "\t"), '', $result);
$fileContents = utf8_for_xml(trim(str_replace('"', "'", $fileContents)));
$simpleXml = simplexml_load_string($fileContents,'SimpleXMLElement',LIBXML_NOCDATA);
$json = json_encode( $simpleXml->channel );
echo $json;
?>
